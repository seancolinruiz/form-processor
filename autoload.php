<?php
/**
* Use the Standard PHP Library (SPL) to define class autoload
* functionality related to this project.
*
* @author Sean Colin Ruiz
* @license 
*/
	namespace FormProcessor;

	/**
	* This anonymous functions defines where to look for our
	* form processing classes: in a subdirectory named "classes."
	*
	* @link https://goo.gl/I9Dz1F
	*/
	spl_autoload_register( function( $class_name ){
		$class_name = str_replace( '\\', DIRECTORY_SEPARATOR, $class_name );

		//Get the filepath and concatenate the file
		$filepath = dirname( __FILE__ ) . '/classes'; 
		$file = $filepath . '/' . $class_name . '.php';

		if( !file_exists( $file ) ){
			return( FALSE );
		}

		include( $file );
	});
