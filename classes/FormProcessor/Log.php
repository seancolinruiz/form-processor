<?php
	namespace FormProcessor;

	use \PDO;

	/**
	*  Uses throughout the script to keep track of log messages, then send to a log file
	*/
	class Log{
		private static $log_messages = array(); //Store the log messages throughout the life of this script
		private static $log_messages_db = array(); //Store an associative array of DB column names and values

		/**
		* Loop through the form fields and log them as messages for the database.
		* Important note: There must be a column corresponding to each field name.
		*
		* @param Form $form A Form instance.
		*/
		public static function log_fields_db( Form $form ){
			foreach( $form->fields_array() as $field_name => $field_value ){
				self::log_message_db( $field_name, $field_value );
			}
		}

		/**
		* Adds a log message to the log messages array
		*/
		public static function log_message_db( $column_name, $column_value ){
			if( is_array( $column_value ) ){
				$column_value = implode( ',', $column_value );
			}

			self::$log_messages_db[ $column_name ] = $column_value;
		}

		/**
		* Adds a log message to the log messages array
		*/
		public static function log_message( $log_message ){
			array_push( self::$log_messages, $log_message );
		}


		/**
		* Write the log messages to the file system
		*
		* @todo Report on failure.
		*/
		public static function log_output_to_file( $filepath = '' ){
			if( empty( $filepath ) ){
				$filepath = Settings::get( 'filepath_log' );
			}

			if( file_exists( $filepath ) && is_writeable( $filepath ) ){
				$result = file_put_contents( $filepath, implode( "\n", self::$log_messages ) , FILE_APPEND );
			}
		}

		/**
		* Write the log messages to the database 
		*
		* @todo handle the exception
		* @todo handle fail to execute DB query
		*/
		public static function log_output_to_database(){
			$db_dsn = Settings::get( 'db_dsn' );
			$db_user = Settings::get( 'db_user' );
			$db_password = Settings::get( 'db_password' );
			$db_table = Settings::get( 'db_table' );

			try{ //Try to connect to the database
				$db = new PDO( $db_dsn, $db_user, $db_password );
			} catch( PDOException $e ){
				//Fail silently
			}

			if( $db ){ //Check that there is a valid DB connection
				$values = array(); //Create an empty array to hold the future values for the database

				//Loop through the log entries and set the parameter values according to their corresponding log entry
				foreach( self::$log_messages_db as $column_name => $log_message ){
					$column_names[] = $column_name;
					$bind_parameters[] = ':' . $column_name;

					$values[ ':' . $column_name ] = $log_message; //Set the bind parameter value
				}

				//Create the SQL string, using imploded arrays of column names and bind parameters
				$sql = 'INSERT INTO ' . $db_table . '( '; 
				$sql .= implode( ', ', $column_names );
				$sql .= ') VALUES (';
				$sql .= implode( ', ', $bind_parameters );
				$sql .= ')';

				$statement = $db->prepare( $sql ); //Prepare the statement

				//Loop through the values and set the bind parameters on the prepared statement
				foreach( $values as $bind_parameter => &$value ){ 
					$statement->bindParam( $bind_parameter, $value );
				}

				$result = $statement->execute(); 

				if( $result === FALSE ){
					//Fail silently
				}
			}
		}

		//Send the output to the screen
		public static function log_output_to_screen(){
			echo implode( "\n", self::$log_messages );
		}

		//Add a log message when the script started
		public static function log_message_start_time(){
			$date_format = Settings::get( 'log_date_format' );
			self::log_message( 'Starting on: ' . date( $date_format ) . '...' . "\n" );
		}

		//Add a log message when the script ended
		public static function log_message_end_time(){
			$date_format = Settings::get( 'log_date_format' );
			self::log_message( 'Ending on: ' . date( $date_format ) . '...' . "\n" );
		}

		//Add a log message from an array
		public static function log_message_array_data( $text, $array_data ){
			$log_message = '';

			foreach( $array_data as $key => $value ){
				$log_message .= $key . ': ' . $value . "\n";
			}

			self::log_message( $text . "\n" . $log_message );
		}

		//Log the original raw post data
		public static function log_message_raw_post(){
			self::log_message_array_data( 'Raw HTTP POST data: ', $_POST );
		}


		//Log the form data
		public static function log_message_form_data( $form ){
			if( $form instanceof Form ){
				self::log_message_array_data( 'Form data: ', $form->data );
			}
		}
	}

