<?php
	namespace FormProcessor;

	use FormProcessor\FieldFilter;
	use FormProcessor\FieldValidation;

	/**
	* This class defines the logical structure of a generalized form field.
	* The simplest form field has a name and a value (field_name and value). 
	* However, a form field may also have a human readable name 
	* (field_name_user_friendly), an array of validation methods
	* (validation_methods), and an array of filters (filter_methods).
	*
	* Filter methods and validation methods may be added as arrays at 
	* the time of instantiation, or they may be added via the 
	* add_filter_method and add_validation_method methods.
	*
	* The "validate" method will loop through all of the validation 
	* methods and attempt to validate the field.
	*
	* As a result of validation, a form field may have errors.
	* These errors are accessible via the class's $errors property.
	*/
	class Field{
		protected $errors = array(); //An array of errors with this form field
		protected $field_name = ''; //The name attribute of the form field
		protected $field_name_user_friendly = ''; //The user-friendly name of the field
		protected $filter_methods = array(); //An array of filter methods
		protected $send = TRUE; //Flag whether or not to send this field
		protected $trim = TRUE; //Flag whether or not to trim this field
		protected $validation_methods = array(); //An array of validation methods
		protected $value; //The value of the form field

		public function __construct( $field_name, $field_name_user_friendly, $validation_methods, $filter_methods, $send ){
			$this->field_name = $field_name;
			$this->field_name_user_friendly = $field_name_user_friendly;
			$this->send = (boolean) $send;

			foreach( $filter_methods as $filter_method ){ //Loop throgh and add each fitler method
				$this->add_filter_method( $filter_method );
			}
			
			foreach( $validation_methods as $validation_method ){ //Loop throgh and add each fitler method
				$this->add_validation_method( $validation_method );
			}
		}

		public function __get( $property ){
			$value = NULL; //Default return value

			switch( $property ){
				case 'field_name':
				case 'field_name_user_friendly':
				case 'filter_methods':
				case 'errors': //Array
				case 'send': //Boolean
				case 'trim': //Boolean
				case 'validation_methods':
					$value = $this->$property;
					break;

				case 'value': 
					$value = $this->$property;
					break;
			}

			return( $value );
		}

		public function __toString(){
			return( $this->value );
		}

		public function __set( $property, $value ){
			switch( $property ){
				case 'send': //Cast value to boolean
				case 'trim': //Deliberate pass-through
					$this->$property = (bool) $value;
					break;

				case 'value': //Set the value of the property
					$this->$property = $value; 
					break;
			}
		}

		/**
		* This method adds an error message to the array of form field errors messages
		*
		* @param string $message The error message to add to the array of error messages
		*/
		public function add_error_message( $message ){
			array_push( $this->errors, $message );
		}

		/**
		* This method adds a form field filter to the array of form field filter.
		*
		* @param string $filter_method The name of a public form_field_filter method
		*/
		public function add_filter_method( $filter_method ){
			array_push( $this->filter_methods, $filter_method );
		}

		/**
		* This adds a form field validation method to array of validation methods.
		*
		* @param string $validation_method The name of a public form_field_validation method
		*/
		public function add_validation_method( $validation_method ){
			array_push( $this->validation_methods, $validation_method );
		}

		/**
		* This method checks if there are any errors in the error array, then it returns
		* TRUE if there are errors and FALSE if there are not.
		*
		* @return boolean Returns TRUE if there are errors, otherwise it returns FALSE
		*/
		public function is_error(){
			return( (bool)( count( $this->errors ) ) );
		}

		/**
		* This method loops through the array of filter methods and invokes them each in turn.
		*/
		public function filter(){
			if( $this->trim ){ //One off trim based upon class property
				$class_name = 'FormProcessor\FieldFilter\trim';
				$interface_name = 'FormProcessor\FieldFilter\Interfaces\iFieldFilter';

				if( class_exists( $class_name ) ){ //Check if the class exists
					if( in_array( $interface_name, class_implements( $class_name ) ) ){
						$class_name::filter( $this );
					}
				}
			}

			foreach( $this->filter_methods as $filter_method ){
				$class_name = 'FormProcessor\FieldFilter\\' . $filter_method;
				$interface_name = 'FormProcessor\FieldFilter\Interfaces\iFieldFilter';

				if( class_exists( $class_name ) ){ //Check if the class exists
					if( in_array( $interface_name, class_implements( $class_name ) ) ){
						$class_name::filter( $this );
					}
				}
			}
		}

		/**
		* This method loops through the array of validation methods and invokes them each in turn.
		*/
		public function validate(){
			foreach( $this->validation_methods as $validation_method ){
				$class_name = 'FormProcessor\FieldValidate\\' . $validation_method;
				$interface_name = 'FormProcessor\FieldValidate\Interfaces\iFieldValidate';

				if( class_exists( $class_name ) ){ //Check if the class exists
					if( in_array( $interface_name, class_implements( $class_name ) ) ){
						$class_name::validate( $this );
					}
				}
			}
		}
	}
