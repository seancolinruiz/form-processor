<?php
	namespace FormProcessor;

	/**
	* Sends the form to the destination using the specified method
	*/
	class Submit{
		//Changes how the form is sent, used as parameters to the submit_method method of the form_submit static class
		const SUBMIT_METHOD_HTTP_STREAM_CONTEXT = 0;
		const SUBMIT_METHOD_CURL = 1;
		const SUBMIT_METHOD_CURL_JSON = 2;

		private static $method = self::SUBMIT_METHOD_CURL;

		public static function get_method( ){
			return( static::$method );
		}

		public static function set_method( $method ){
			static::$method = $method;
		}

		/**
		* Send the form via HTTP stream context
		*
		* @return string reteurn the value from the Web service
		*/
		protected static function submit_http_stream_context( $form ){
			$form_fields = $form->fields_array();
			$url_destination = $form->url_destination;

			Log::log_message_array_data( 'What was sent to ' . $url_destination . ': ', $form_fields );
			Log::log_message_db( 'payload_raw', print_r( $form_fields, TRUE ) );

			$form_fields = http_build_query( $form_fields );
			Log::log_message( 'Raw post to destination: ' . "\n" . $form_fields );
			Log::log_message_db( 'payload_data', $form_fields );

			$stream_context_options = array(
				'http' => array(
					'method' => 'POST',
					'header' => "Content-type: application/x-www-form-urlencoded\r\n" .
						    "Content-length: " . strlen( $form_fields ) . "\r\n",
					'content' => $form_fields
				)
			);

			$stream_context = stream_context_create( $stream_context_options );
			$response = file_get_contents( $url_destination, null, $stream_context );

			return( $response );
		}

		/**
		* Send the form via cURL and HTTP POST
		*
		* @return array with the response body and HTTP response code
		*/
		protected static function submit_curl( $form ){
			$form_fields = $form->fields_array(); //Get the form
			$url_destination = $form->url_destination; //Get the post URL

			Log::log_message_array_data( 'What was sent to ' . $url_destination . ': ', $form_fields );
			Log::log_message_db( 'payload_raw', print_r( $form_fields, TRUE ) );

			$form_fields = http_build_query( $form_fields ); //Build the HTTP POST string
			Log::log_message( 'Raw post to destination: ' . "\n" . $form_fields );
			Log::log_message_db( 'payload_data', $form_fields );

			$ch = curl_init(); //Initialize the cURL handler
			curl_setopt($ch, CURLOPT_URL, $url_destination);
			curl_setopt($ch, CURLOPT_POST, TRUE);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $form_fields);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_VERBOSE, TRUE);
			curl_setopt($ch, CURLOPT_HEADER, TRUE);

			curl_setopt($ch, CURLOPT_HTTPHEADER, array( 'Content-Type: application/x-www-form-urlencoded' ) );

			$response = curl_exec($ch);
			$response_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

			curl_close($ch);

			return( array( 'body' => $response, 'http_response_code' => $response_code ) );
		}

		/**
		* Send the form via cURL, HTTP POST, and JSON
		*
		* @return string reteurn the value from the Web service
		*/
		protected static function submit_curl_json( $form ){
			$form_fields = $form->fields_array(); //Get the form
			$url_destination = $form->url_destination; //Get the post URL

			Log::log_message_array_data( 'What was sent to ' . $url_destination . ': ', $form_fields );
			Log::log_message_db( 'payload_raw', print_r( $form_fields, TRUE ) );

			$form_fields = json_encode( $form_fields ); //JSON encode the data
			Log::log_message( 'Raw post to destination: ' . "\n" . $form_fields );
			Log::log_message_db( 'payload_data', $form_fields );

			$ch = curl_init(); //Initialize the cURL handler
			curl_setopt($ch, CURLOPT_URL, $url_destination);
			curl_setopt($ch, CURLOPT_POST, TRUE);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $form_fields);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array( 'Content-type: application/json' ) );
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			$response = curl_exec($ch);
			curl_close($ch);

			return( $response );
		}

		//Send the form, returns false if it was unable to send
		public static function submit( $form ){
			switch( self::$method ){
				case self::SUBMIT_METHOD_HTTP_STREAM_CONTEXT:
					$response = self::submit_http_stream_context( $form );
					break;

				case self::SUBMIT_METHOD_CURL:
					$response = self::submit_curl( $form );
					break;

				case self::SUBMIT_METHOD_CURL_JSON:
					$response = self::submit_curl_json( $form );
					break;

				default: 
					$response = FALSE; //Default return value
					break;
			}

			return $response; 
		}
	}
