<?php
	namespace FormProcessor;

	/**
	* Define's a data structure for email messages. 
	* All the properties are directly read/write, but there are some measure in place to guarentee integrity
	* This class also defines a method for sending the e-mail, and additional methods may be later defined.
	*
	*/
	class Email{
		protected $to = '';
		protected $from = '';
		protected $reply_to = '';
		protected $cc = '';
		protected $bcc = '';
		protected $subject = '';
		protected $body = '';
		protected $headers = array();

		//Govern access to a whitelist of class properties
		public function __get( $property ){
			switch( $property ){ 
				case 'to': //Note: intentionally pass-through
				case 'from':
				case 'reply_to':
				case 'cc':
				case 'bcc':
				case 'subject':
				case 'body':
				case 'headers':
					$value = $this->$property;
					break;

				default: 
					$value = NULL; //Default return value
			}

			return( $value );
		}

		//Set the values of a white list of properties
		public function __set( $property, $value ){
			switch( $property ){
				case 'to':
				case 'from':
				case 'reply_to':
				case 'cc':
				case 'bcc':
				case 'subject':
				case 'body':
					$this->$property = (string) $value; //Cast to string
					break;

				case 'headers':
					$this->$property = (array) $value; //Cast to array
					break;
			}
		}

		//Send the e-mail
		public function send(){
			$this->add_headers(); //Add the from, cc, and bcc headers

			if( function_exists( 'wp_mail' ) ){ //Try to send through wordpress, first
				$status = wp_mail( $this->to, $this->subject, $this->body, implode( "\r\n", $this->headers ) );
			} else { //Send via PHP
				$status = mail( $this->to, $this->subject, $this->body, implode( "\r\n", $this->headers ) );
			}

			$log_data = array(
				'To' => $this->to,
				'Subject' => $this->subject,
				'Body' => $this->body,
				'Headers' => implode( "\r\n", $this->headers )
			);
		}

		public function add_header( $header ){ //Push a header onto the array of headers
			array_push( $this->headers, (string) $header );
		}

		protected function add_headers(){
			if( !empty( $this->from ) ){ //Add the from property
				$this->add_header( 'From: ' . $this->from );
			}

			if( !empty( $this->reply_to ) ){ //Add the from property
				$this->add_header( 'Reply-to: ' . $this->reply_to );
			}

			if( !empty( $this->cc ) ){ //Add the CC property
				$this->add_header( 'Cc: ' . $this->cc );
			}

			if( !empty( $this->bcc ) ){ //Add the BCC property
				$this->add_header( 'Bcc: ' . $this->bcc );
			}
		}
	}
