<?php
	namespace FormProcessor\FieldValidate\Interfaces;
	use FormProcessor\Field;

	interface iFieldValidate{
		public static function validate( Field $form_field );
	}
