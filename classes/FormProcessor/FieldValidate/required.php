<?php
	namespace FormProcessor\FieldValidate;
	use FormProcessor\Field;

	class required implements Interfaces\iFieldValidate{
		public static function validate( Field $form_field ){
			$value = $form_field->value; //Get the field value
                        $field_name_user_friendly = $form_field->field_name_user_friendly; //User-friendly name

                        if( empty( $value ) ){
                                $form_field->add_error_message( 'The ' . $field_name_user_friendly . ' field is required.' );
                        }
		}
	}
