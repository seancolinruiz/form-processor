<?php
	namespace FormProcessor;

	class Settings{
		/**
		* Define an array of settings and their default values.
		* These settings may only be read/write through accessor methods. 
		*/
		private static $registry = array(
			'date_format_file' => 'l jS \of F Y h:i:s A', //A valid format string for date()
		 	'db_dsn' => '', //http://php.net/manual/en/class.pdo.php
		 	'db_password' => '',
		 	'db_table' => '',
		 	'db_user' => '',
		 	'filepath_log' => 'log.txt', //Filepath to the log file
			'log_to_database' => FALSE, //Flag logging to DB
			'log_to_filesystem' => FALSE, //Flag logging to filesystem
			'redirect' => TRUE //Flag redirecting after submission
		);
		
		/**
		* Set to private, to make sure this object can't be instantiated or cloned.
		*/
		private function __construct(){}
		private function __clone(){}

		/**
		* Get the value of a setting from the registry
		*/
		public static function get( $setting, $default = NULL ){
			$value = $default;

			if( isset( self::$registry[ $setting ] ) ){
				$value = self::$registry[ $setting ];
			}

			return( $value );
		}

		/**
		* Set the value of a setting in the registry.
		*
		* @param mixed $setting Name of the setting whose value is being
		* set; or an array of key value pairs:
		* 	$setting = array(
		*		'date_format_file' => (string) Date format string
		*		'db_dsn' => (string) PDO datasource name
		*		'db_password' => (string) PDO database password
		*		'db_table' => (string PDO database table
		*		'db_user' => (string) PDO database username
		*		'filepath_log' => (string) Filepath to the log file
		*		'log_to_database' => (boolean) FALSE, //Flag logging to DB
		*		'log_to_filesystem' => (boolean) TRUE, //Flag logging to filesystem
		*		'redirect' => (boolean) TRUE //Flag redirecting after submission
		*	)
		* @param mixed $value Value for the setting. Will be ignored if
		* the first parameter is an array.
		*/
		public static function set( $setting, $value = '' ){
			if( is_array( $setting ) ){
				self::set_array( $setting );
			} else if( isset( self::$registry[ $setting ] ) ){
				self::$registry[ $setting ] = $value;
			}
		}

		/**
		* Utility function for setting an array of settings.
		*
		* @param array $settings An array containing form processor settings.
		*/
		public static function set_array( $settings = array() ){
			//Loop through the passed in settings and set them
			foreach( $settings as $option => $value ){
				Settings::set( $option, $value );
			}
		}
	}
