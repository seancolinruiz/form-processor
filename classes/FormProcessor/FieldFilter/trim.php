<?php
	namespace FormProcessor\FieldFilter;
	use FormProcessor\Field;

	class trim implements Interfaces\iFieldFilter{
		/**
		* Trim the value of the form field
		*
		* @static
		*/
		public static function filter( Field $form_field ){
			$value = $form_field->value; //Get the field value
			$form_field->value = trim( $value );
		}
	}
