<?php
	namespace FormProcessor\FieldFilter\Interfaces;
        use FormProcessor\Field;

	interface iFieldFilter{
		public static function filter( Field $form_field );
	}
