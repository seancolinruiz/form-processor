<?php
	namespace FormProcessor;

	/**
	* Stores user input and the registered forms. Also processes the forms.
	*/
	class FormProcessor{
		/**
		* This is the name of the form field that FormProcessor will check 
		* to identify the name of the form. This is one of the most important 
		* properties and without it FormProcessor cannot map the HTML form 
		* on the front-end to the PHP Form object on the back-end.
		*
		* Technically defined: This is the VALUE of the NAME attribute for the 
		* HTML form field (e.g. INPUT, SELECT, etc.) that will map the HTML form 
		* to the PHP Form object.

		* Note: The default value is "action" because that works well with 
		* WordPress. But that may be changed by simply setting the value on 
		* the instance property, e.g. $form_processor->form_field_name = 'route';
		*/
		protected $form_field_name = 'action';

		protected $forms = array(); //Will store the defined forms

		//Holds HTTP POST/GET and file upload information 
		protected $post = array();
		protected $get = array();
		protected $files = array();

		/**
		* Takes and sets the values of HTTP POST, HTTP GET, and uploaded file input.
		*
		* @param array $post An array of HTTP POST data
		* @param array $get An array of HTTP GET data
		* @param array $files An array of uploaded files
		*/
		public function __construct( $post = array(), $get = array(), $files = array() ){
			$this->post = (array) $post;
			$this->get = (array) $get;
			$this->files = (array) $files;
		}

		/**
		* Govern read access to a whitelist of instance properties.
		*
		* @param string $property The name of the instance property to read.
		*/
		public function __get( $property ){
			$value = NULL; //Default return value

			switch( $property ){
				case 'form_field_name':
					$value = $this->$property;
					break;

				case 'post': //Intentional pass-through
				case 'get':
				case 'files':
					$value = (array) $this->$property;
					break;
			}

			return( $value );
		}

		/**
		* Govern write access to a whitelist of instance properties.
		*
		* @param string $property The name of the property to set.
		* @param mixed $value The value of the property to set.
		*/
		public function __set( $property, $value ){
			switch( $property ){
				case 'form_field_name':
					$this->$property = (string) $value;
					break;

				case 'post': //Intentional pass-through
				case 'get':
				case 'files':
					$this->$property = (array) $value;
					break;
			}
		}

		/**
		* Find the form that is supposed to process the user input
		*
		* @todo Need to build in logic to handle both POST/GET
		*/
		public function lookup_form(){
			$form = FALSE; //Default return value
			$form_field_name = $this->form_field_name;
			$input = $this->post;

			if( isset( $input[ $form_field_name ] ) ){
				$form_name = $input[ $form_field_name ]; //Determine the form action

				if( isset( $this->forms[ $form_name ] ) ){
					$form = $this->forms[ $form_name ];
				} else {
					Log::log_message( 'Error processing form: No form found with the name "' . $form_name . '."' );
				}
			} else {
				Log::log_message( 'Error processing form: No form_field_name found. Looking for: ' . $form_field_name );
			}

			return( $form );
		}

		/**
		* This is main form processing method. Here's what it does:
		* 1. Get the stored user input.
		* 2. Find the form that is supposed to be processed.
		* 3 Filter/validate the form.
		* 4. Submit it to the CRM or populate an array of errors.
		* 5. Redirect the user.
		*
		* @param boolean $filter This parameter flags whether or not the form fields 
		* should be immediately filtered.
		* @param boolean $validate This parameter flags whether or not the form fields 
		* should be immediately validated
		* @return Form|boolean This method will return the form associated with the input, 
		* or false it is unable to find one.
		*/
		public function process( $filter = TRUE, $validate = TRUE ){
			$form = FALSE; //Default return value;

			Log::log_message_start_time(); //Log when the script starts
			Log::log_message_raw_post(); //Log the original raw post data

			$form = $this->lookup_form(); //Find the form
			$input = $this->post; //Get the stored input

			if( $form ){
				//Pass form input to individual form
				$form->input( $input );

				if( $filter ){ //Optionally filter the form fields
					$form->filter();
				}

				if( $validate ){ //Optionally validate the form fields
					$form->validate( FALSE );
				}

				if( $form->is_valid() ){
					//Send the form to its destination
					$response = Submit::submit( $form );

					//Log the response from the CRM
					Log::log_message_array_data( 'CRM response: ' . "\n", (array) $response );

					//Log the fields in the database
					Log::log_fields_db( $form );
				} else {
					//Loop through the fields and add them to the session
					foreach( $form->fields as $field_name => $form_field ){ 
						$_SESSION[ 'form_defaults' ][ $field_name ] = $form_field->value;
					}
				}
			} else {
				Log::log_message( 'No form found while processing.' );
			}

			//Log when the script ends and write the log file to the file system
			Log::log_message_end_time();

			if( (boolean) Settings::get( 'log_to_filesystem' ) ){
				Log::log_output_to_file( Settings::get( 'filepath_log' ) );
			}

			if( (boolean) Settings::get( 'log_to_database' ) ){
				Log::log_output_to_database();
			}
			
			if( (boolean) Settings::get( 'redirect' ) ){
				$this->redirect( $form );
			}
		}

		/**
		* A protected method that invokes the form's return_url method, 
		* and then redirects the user.
		*
		* @param Form $form Instance of a form
		*/
		protected function redirect( Form $form ){
			$return_url = $form->return_url(); //Figure out where to send the user back to

			if( $return_url ){ //Redirect, if there is a need for it
				//Redirect the user off of the page
				header( 'Location: ' . $return_url, true, 302 );
				exit;
			}
		}

		/**
		* Used to register a form: A form must be registered with the Forms
		* class, in order for it to be available for form processing.
		*
		* @param Form $form Instance of a form
		*/
		public function register_form( Form $form ){
			//Add the form to the array of forms
			if( $form instanceof Form ){
				$form->form_processor = $this;
				$this->forms[ $form->name ] = $form;
			}
		}
	}
