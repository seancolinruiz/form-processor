<?php
	namespace FormProcessor;

	use \ArrayAccess;

	/**
	* This class defines the logical structure of a generalized form. 
	* The idea behind this class is simple: a form has fields and it goes somewhere.
	*
	* Each form requires a unique "name" that can be used to reference it and look it up.
	* Once the form name has been specified, it cannot be changed.
	*
	* The class defines methods for adding fields (e.g. add_field, add_field_array, add_field_object)
	* and it also includes a way to add fields when instantiating a new form.
	*
	* The form also has public methods to validate and filter the form fields, these methods
	* are "validate" and "filter" respectively. These methods delegate the duty of validating
	* and filtering the individual form fields to the individual form fields themselves.
	*
	* @property array $input This is the raw input
	*
	* @property array $fields An array of Field objects
	* 
	* @property FormProcessor $form_processor Used to contain an instance of 
	* FormProcessor
	*
	* @property string $name The name of the form (used to ID it)
	*
	* @property boolean $sessions Give the user of the class the option to use 
	* the URL for sending errors.
	*
	* @property string $url_destination Where the form is going to be sent
	* 
	* @property string $url_error Where the user should be redirected when the 
	* form is unsuccessful
	* 
	* @property string $url_success Where the user should be redirected when the 
	* form succeeds
	*/
	class Form implements ArrayAccess{
		protected $input = array();
		protected $fields = array();
		protected $form_processor;
		protected $name;
		protected $sessions = TRUE;
		protected $url_destination; 
		protected $url_error; 
		protected $url_success;

		/**
		* Initialize the form object.
		*
		* @param string	$name This is the name (identifier) for the form
		*
		* @param string $url_destination This is where the form is being sent
		*
		* @param string $form_fields This is an ooptional array of form_field 
		* objects, used to build the form
		*/
		public function __construct( $name, $url_destination, $form_fields = array() ){
			$this->name = $name; //Register the form name with the object
			$this->url_destination = $url_destination; //Where the form is being sent

			if( count( $form_fields ) ){
				foreach( $form_fields as $form_field ){ //Loop through and add the form fields
					//Use the appropriate method, depending on the type
					if( is_array( $form_field ) ){ 
						$this->add_field_array( $form_field );
					} else if( $form_field instanceof Field ){
						$this->add_field_object( $form_field );
					}
				}
			}
		}
		
		public function __get( $property ){
			$value = NULL; //Default return value

			switch( $property ){
				case 'input': //Deliberate pass-through
				case 'fields': 
				case 'form_processor':
				case 'name':
				case 'url_destination':
				case 'url_error':
				case 'url_success':
					$value = $this->$property;
					break;

				default:
			}

			return( $value );
		}

		public function __set( $property, $value ){
			switch( $property ){
				case 'form_processor':
					//Check the type of the value before assigning it
					if( $value instanceof FormProcessor ){
						$this->$property = $value;
					}
					break;

				case 'sessions':
					$this->$property = (bool) $value;
					break;

				case 'url_error':
				case 'url_success':
					$this->$property = (string) $value;
					break;

				default:
			}
		}

		/**
		* This is a method that is used to add form fields to the form.
		*
		* @param string $field_name This is the name of the form field,
		* it must correspond to the HTML name attr.
		*
		* @param string $field_name_user_friendly This the user-friendly name, 
		* used to communicate to the user
		*
		* @param array $filter_methods This is an optional array of filter methods.
		* 
		* @param array $validation_methods This is an optional array of validation 
		* methods. 
		*/
		public function add_field( $field_name, $field_name_user_friendly = '', $validation_methods = array(), $filter_methods = array(), $send = TRUE ){
			$form_field = new Field( $field_name, $field_name_user_friendly, $validation_methods, $filter_methods, $send );
			$this->fields[ $field_name ] = $form_field;
		}

		/**
		* This method takes an associative array of values for the form_field 
		* constructure, creates the object, and then adds it the array of 
		* form fields.
		*
		* @param array $form_field An associative array with at least one 
		* field_name key, must be unique for the form.
		*/
		public function add_field_array( $form_field ){
			if( is_array( $form_field ) && isset( $form_field[ 'field_name' ] ) ){
				//Default values
				$field_name = $form_field[ 'field_name' ];
				$field_name_user_friendly = $form_field[ 'field_name' ];
				$filter_methods = array();
				$validation_methods = array();

				if( isset( $form_field[ 'field_name_user_friendly' ] ) ){
					$field_name_user_friendly = $form_field[ 'field_name_user_friendly' ];
				}

				if( isset( $form_field[ 'filter_methods' ] ) ){
					$filter_methods = $form_field[ 'filter_methods' ];
				}

				if( isset( $form_field[ 'validation_methods' ] ) ){
					$validation_methods = $form_field[ 'validation_methods' ];
				}

				//Instantiates the Field and adds it to the array of form fields
				$this->add_field( $field_name, $field_name_user_friendly, $validation_methods, $filter_methods  );
			}
		}

		public function add_field_arrays( $form_fields ){
			foreach( $form_fields as $form_field ){
				$this->add_field_array( $form_field );
			}
		}

		/**
		* This method adds a Field object to the array of form fields
		*
		* @param object $form_field This is an instance of the Field class
		*/
		public function add_field_object( $form_field ){
			if( $form_field instanceof Field ){
				$this->fields[ $this->field_name ] = $form_field;
			}
		}

		/**
		* This loops through the form fields and returns an array of errors
		*
		* @return array This will be an array of errors.
		*/
		public function errors(){
			$errors = array();

			//Loop through the form fields and check them for errors
			foreach( $this->fields as $name => $form_field ){ 
				if( $form_field->is_error() ){ //Check if the field has errors
					//Merge the array of errors into this array of all form errors
					$errors = array_merge( $errors, $form_field->errors );
				}
			}

			return( $errors );
		}

		/**
		* Simple utility method to return the name of the form.
		* 
		* @return string Returns the name of the form
		*/
		public function get_form_name(){
			return( $this->name );
		}

		/**
		* This is a utility function for converting the array of Field objects 
		* to an array of key value pairs.
		*
		* @param boolean $values_only Flag whether or not the full form_field 
		* object should be returned, or just the values.
		*
		* @param boolean $sendable_fields_only Flag whether to return all fields or
		* just the sendable fields.
		*
		* @return array This returns an array of key => value pairs related 
		* to form fields
		*/
		public function fields_array( $values_only = TRUE, $sendable_fields_only = FALSE ){
			$fields = array();

			foreach( $this->fields as $form_field ){
				//Either send everything or just sends the sendable fields
				if( !$sendable_fields_only || ( $sendable_fields_only && $form_field->send ) ){
					if( $values_only ){
						$fields[ $form_field->field_name ] = $form_field->value;
					} else {
						$fields[ $form_field->field_name ] = $form_field;
					}
				}
			}
            
			return( $fields );
		}

		/**
		* This method loops through all of the form fields and invokes their 
		* "filter" methods.
		*/
		public function filter(){
			$form_fields = $this->fields;

			foreach( $form_fields as $field_name => $form_field ){
				$form_field->filter();
			}
		}

		/**
		* This method take an assocaitve array of input and applies the values to 
		* field_names with names corresponding to the key.
		*
		* @param array $input An associative array of key => value pairs. 
		* They keys must be named after field_names in order for them to be 
		* accepted as valid input.
		*/
		public function input( $input ){
			$this->input = $input; //Store the raw input

			//Loop through the input and update the value of the form fields
			foreach( $input as $field_name => $field_value ){ 
				//Check for a corresponding field_name
				if( isset( $this->fields[ $field_name ] ) ){ 
					//Set the field value
					$this->fields[ $field_name ]->value = $field_value;
				}
			}
		}

		/**
		* This method loops through all of the form fields and checks them each 
		* in turn, to see if any of them have any errors. If no errors are found, 
		* then this method returns TRUE otherwise it returns FALSE.
		*
		* @return boolean TRUE if there are no errors and the form is valid, 
		* otherwise it returns FALSE.
		*/
		public function is_valid( ){
			$valid = TRUE; //Default return value

			$form_fields = $this->fields; //Get the form's fields

			//Loop through each form field and throw a flag if one is in error
			foreach( $form_fields as $form_field ){
				if( $form_field->is_error() ){
					$valid = FALSE;
				}
			}

			return( $valid );
		}

		//Check if a form field with the specified name exists
		public function offsetGet( $name ){
			return( $this->fields[ $name ] );
		}

		//Check if a form field with the specified name exists
		public function offsetExists( $name ){
			return( isset( $this->fields[ $name ] ) );
		}

		//Non-operational, we don't want to be ale to set values
		public function offsetSet( $name, $value ){
			//No-op...
		}

		//Non-operational, we don't want to be able to unset values
		public function offsetUnset( $offset ){
			//No-op...
		}

		//Returns the return URL, depending on whether the form is valid or invalid
		public function return_url( ){
			if( $this->is_valid() ){
				$return_url = $this->url_success();
			} else { 
				$return_url = $this->url_error();
			}

			return( $return_url );
		}

		protected function url_success( ){
			$return_url = $this->url_success;

			if( isset( $this->input[ 'return_url' ] ) ){
				$return_url = $this->input[ 'return_url' ];
			}

			//Use the provided return url, if it's not empty
			return( $return_url );
		}

		protected function url_error( ){
			$return_url = $this->url_error;

			if( isset( $this->input[ 'return_url_errors' ] ) ){
				$return_url = $this->input[ 'return_url_errors' ];
			}

			if( empty( $return_url ) ){
				//Otherwise use the HTTP referer
				$return_url = $_SERVER[ 'HTTP_REFERER' ];
			}

			//Toggle between using sessions or a URL to pass error information back
			if( $this->sessions && session_id() ){ //Check if we're flagged to use and have available
				$_SESSION[ 'errors' ] = $this->errors();
			} else {
				$return_url = $this->url_lorm_errors(); //Create the return URL with errors in the query string
			} 

			return( $return_url );
		}

		/**
		* This method uses the HTTP referrer in order to send the visitor back to the page with errors;
		* the errors are sent as GET variables via the URL.
		*
		* @return string This will return the HTTP referrer with any errors set as GET variables
		*/
		public function url_form_errors( ){
			if( stripos( $return_url, '/' ) === 0 ){ //Convert root relative URLs to absolute URLs
				$return_url = 'http://' . $_SERVER[ 'HTTP_HOST' ] . $return_url;
			}

			//Convert the return URL into an array of its components
			//Parse the URL into an array, so we can modify the original query string
			$return_url = parse_url( $return_url ); 
			$return_url = sprintf( '%s://%s%s', $return_url[ 'scheme' ], $return_url[ 'host' ], $return_url[ 'path' ] );

			//Use to hold URL variables we may alter
			$query_array = array();

			$query_string = parse_url( $return_url, PHP_URL_QUERY ); //Get the original query string

			if( $query_string ){ //Check for the presence of a query string
				parse_str( $query_string, $query_array ); //Create an array out of the query string

				if( isset( $query_array[ 'errors' ] ) ){ //Check for old errors
					unset( $query_array[ 'errors' ] ); //Remove the old error messages
				}
			}

			$errors = $this->errors(); //Create an array of errors with the form

			if( count( $errors ) ){ //Add any new error messages
				//Remove any old errors and add the new ones in, in one step
				$query_array[ 'errors' ] = $errors;
			}

			if( count( $query_array ) ){ //Check if there's still a query array, so we can add it back on
				//Rebuild the query string
				$query_string = http_build_query( $query_array );

				//Add the query string back on
				$return_url .= '?' . $query_string;
			}

			return( $return_url );
		}
		

		/**
		* This method loops through the form_fields and validates them. It does this by
		* invoking each of the "validate" methods belonging to the Field objects.
		* It will also optionally filter the form_fields, if the $filter option is TRUE.
		*
		* @param boolean $filter Flag whether or not the form fields should be filtered, 
		* prior to validating. This defaults to TRUE, yes they should be filtered.
		*/
		public function validate( $filter = TRUE ){
			$form_fields = $this->fields;

			foreach( $form_fields as $field_name => $form_field ){
				if( $filter ){
					$form_field->filter();
				}

				$form_field->validate();
			}
		}

		/**
		* This loops through the form fields and returns an associative array of values
		*/
		public function values(){
			$values = array();

			foreach( $this->fields as $name => $form_field ){
				$values[ $name ] = $form_field->value;
			}

			return( $values );
		}
	}
