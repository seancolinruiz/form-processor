The directory structure matches the namespaces used within the application.
This allows the autoloader to locate and load the classes based upon the classes
fully qualified classname.
