<?php
/*
*
*/
	namespace FormProcessor;

	if( session_id() == '' ){ //Check if a session exists
		session_start(); //Start a session if it doesn't
	}

	/**
	* Require the autoload script to be in the same directory
	* as the current file. Defines behavior for autoloading 
	* classes.
	*/
	require_once( dirname( __FILE__ ) . '/autoload.php' );
