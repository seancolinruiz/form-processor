<?php
	namespace FormProcessor\Test;

	use FormProcessor\Form;
	use FormProcessor\FormProcessor;
	use \ReflectionClass;

	class FormProcessorTest extends \PHPUnit_Framework_TestCase{
		/**
		* Provide user input for use with testing
		*/
		public function providerFormData(){
			$post = array(
				'first_name' => 'Dizzy',
				'last_name' => 'Gillespie',
				'email' => 'dizzy.gillespie@jazz.com',
				'action' => 'test_form' //Key must match FormProcessor form_field_name
			);

			$get = array(
				'page_id' => 1001
			); 

			$files = array(
				'test_file' => array(
					'name' => 'notes.txt',
					'type' => 'text/plain',
					'size' => '42',
					'tmp_name' => '/tmp/abcxyz123890',
					'error' => ''
				)
			);

			$data[] = array( 
				$post, 
				$get, 
				$files
			);

			return( $data );
		}

		/**
		* Test to see if HTTP POST information is stored and retrieved in the constructor.
		* @dataProvider providerFormData
		*/
		public function test__constructWithPostOnly( $post, $get, $files ){
			$form_processor = new FormProcessor( $post );

			$this->assertSame( $post, $form_processor->post );
		}

		/**
		* Test constructor with all three parameters
		* @dataProvider providerFormData
		*/
		public function test__constructWithPostGetFiles( $post, $get, $files ){
			$form_processor = new FormProcessor( $post, $get, $files );

			$this->assertSame( $post, $form_processor->post );
			$this->assertSame( $get, $form_processor->get );
			$this->assertSame( $files, $form_processor->files );
		}

		/**
		* Test whether the form input can be set via accessor methods
		* @dataProvider providerFormData
		*/
		public function testFormInputAccessorMethods( $post, $get, $files ){
			$form_processor = new FormProcessor();
			$form_processor->post = $post;
			$form_processor->get = $get;
			$form_processor->files = $files;

			$this->assertSame( $post, $form_processor->post );
			$this->assertSame( $get, $form_processor->get );
			$this->assertSame( $files, $form_processor->files );
		}

		/**
		* Take HTTP POST data and instantiate the form processor.
		* Then mock a Form object and register it. Test to see that
		* the Form object was registered.
		*
		* @dataProvider providerFormData
		*/
		public function testRegisterAndLookupForm( $post, $get, $files ){
			$form_processor = new FormProcessor( $post );
			$form_field_name = $form_processor->form_field_name;

			$name = $post[ $form_field_name ]; //Form constructor parameters
			$destination = 'http://example.com';

			//Mock a Form object
			$form = $this->getMockBuilder( '\FormProcessor\Form' );
			$form->setConstructorArgs( array( $name, $destination ) );
			$form->setMethods( array( '__construct' ) );
			$form = $form->getMock();

			$form_processor->register_form( $form );
			$this->assertSame( $form, $form_processor->lookup_form() );
		}
	}
