<?php
	namespace FormProcessor\Test;

	use FormProcessor\Form;

	class FormTest extends \PHPUnit_Framework_TestCase{
		/**
		* Basic form setup.
		*/
		public function providerBaselineForm(){
			$name = 'basic';
			$destination = 'http://example.com/';
			$form_fields = array(
				array( 
					'field_name' => 'full_name',
					'field_name_user_friendly' => 'Full Name'
				),
				array( 
					'field_name' => 'email',
					'field_name_user_friendly' => 'E-mail'
				)
			);

			$data[] = array(  //Add the data set
				$name,
				$destination,
				$form_fields
			);

			return( $data );
		}

		/**
		* Test that the required parameters for the Form constructor work as expected.
		* @dataProvider providerBaselineForm
		*/
		public function test__constructWithNameAndDestination( $name, $destination, $form_fields ){
			$form = new Form( $name, $destination );

			$this->assertEquals( $name, $form->name );
			$this->assertEquals( $destination, $form->url_destination );
		}

		/**
		* Test that the constructor with an array of Form Fields
		* @dataProvider providerBaselineForm
		*/
		public function test__constructWithFieldArray( $name, $destination, $form_fields ){
			$form = new Form( $name, $destination, $form_fields );
			$fields_array = $form->fields_array();
			
			//Check that the form fields were added to the form
			$this->assertArrayHasKey( 'full_name', $fields_array );
			$this->assertArrayHasKey( 'email', $fields_array );
		}

		/**
		* Test whether the validation and filter methods can be set via
		* the form field array constructor parameter.
		* @dataProvider providerBaselineForm
		*/
		public function test__constructWithFieldArrayValidationAndFilter( $name, $destination, $form_fields ){
			//Add the required validation method to both form fields
			$form_fields[ 0 ][ 'validation_methods' ] = array( 'required' );
			$form_fields[ 0 ][ 'filter_methods' ] = array( 'trim' );
			$form_fields[ 1 ][ 'validation_methods' ] = array( 'required' );
			$form_fields[ 1 ][ 'filter_methods' ] = array( 'trim' );

			$form = new Form( $name, $destination, $form_fields ); //Instantiate the form
			$fields_array = $form->fields_array( FALSE ); //Get an array of Field objects

			foreach( $form_fields as $form_field ){ //Loop through the array and verify the methods
				$field_name = $form_field[ 'field_name' ];
				$validation_methods = $form_field[ 'validation_methods' ];
				$filter_methods = $form_field[ 'filter_methods' ];

				$this->assertSame( $validation_methods, $fields_array[ $field_name ]->validation_methods );
				$this->assertSame( $filter_methods, $fields_array[ $field_name ]->filter_methods );
			}
		}

		/**
		* Test that the Form method add_field properly instantiates a field,
		* and adds it to the Form fields array.
		* @dataProvider providerBaselineForm
		*/
		public function test_add_field( $name, $destination, $form_fields ){
			$field_name = 'email';
			$field_name_user_friendly = 'E-mail';
			$validation_methods = array( 'required' );
			$filter_methods = array( 'trim' );
			$send = FALSE;

			$form = new Form( $name, $destination ); //Instantiate the form
			$form->add_field( $field_name, $field_name_user_friendly, $validation_methods, $filter_methods, $send );
			
			$form_field = $form->fields[ $field_name ];

			$this->assertEquals( $field_name, $form_field->field_name );
			$this->assertEquals( $field_name_user_friendly, $form_field->field_name_user_friendly );
			$this->assertSame( $validation_methods, $form_field->validation_methods );
			$this->assertSame( $filter_methods, $form_field->filter_methods );
			$this->assertFalse( $form_field->send );
		}
	}	

