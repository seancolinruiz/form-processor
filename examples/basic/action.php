<?php
	//Define where the form processor lives and include it
	require_once( $_SERVER[ 'DOCUMENT_ROOT' ] . '/form-processor/init.php' );

	//Use two classes from the library
	use \FormProcessor\Form;
	use \FormProcessor\FormProcessor;

	//Instantiate the form with the specified $name and $destination
	$form = new Form( 'basic', 'http://requestb.in/19j1t6d1' );

	//Add the form fields
	$form->add_field( 'first_name', 'First Name', array( 'required' ) );
	$form->add_field( 'last_name', 'Last Name' );
	$form->add_field( 'telephone', 'Telephone', array( 'required' ) );
	$form->add_field( 'zip', 'Zip' );

	$form_processor = new FormProcessor( $_POST ); //Instantiate the form processor with HTTP POST data
	$form_processor->register_form( $form ); //Register the form with the form processor

	//Process the input, validate, filter, and redirect the user
	$form_processor->process();
