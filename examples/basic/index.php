<!DOCTYPE html>
<html>
	<head>
		<title>Basic Example</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	</head>
	<body>
		<div class="container">
			<h1>Basic Example</h1>
			<p>Please fill out the form below:</p>
			<form action="action.php" method="post">
				<div class="form-group">
					<label for="first_name">First Name:</label>
					<input type="text" class="form-control" id="first_name" name="first_name">
				</div>
				<div class="form-group">
					<label for="last_name">Last Name:</label>
					<input type="text" class="form-control" id="last_name" name="last_name">
				</div>
				<div class="form-group">
					<label for="telephone">Telephone:</label>
					<input type="text" class="form-control" id="telephone" name="telephone">
				</div>
				<div class="form-group">
					<label for="zip">Zip:</label>
					<input type="text" class="form-control" id="zip" name="zip">
				</div>
				<button type="submit" class="btn btn-default">Submit</button>
				<input type="hidden" name="action" value="basic">
				<input type="hidden" name="return_url" value="result.html">
			</form>
		</div>
	</body>
</html>
